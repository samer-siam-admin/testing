
import React, { useCallback, useEffect, useState } from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import myStyle from './Styles'
import Splash from './screens/Splash/Splash'
import Details from './screens/Details/Details'
import Login from './screens/Login/Login'


const Stack = createNativeStackNavigator();
export default function App() {

  const [appIsReady, setAppIsReady] = useState(false);
 
  useEffect(() => {
    async function prepare() {
      try {
        await new Promise(resolve => setTimeout(resolve, 5000));
      } catch (e) {
        console.warn(e);
      } finally {
        // Tell the application to render
        setAppIsReady(true);
      }
    }

    prepare();
  }, []);
 
  if (!appIsReady) {
 return <Splash/>
  }
  return <Login/>
  // return (
//     <NavigationContainer>{
//       <Stack.Navigator initialRouteName="Home">
//         <Stack.Screen name="Home" component={Splash} />
//         <Stack.Screen name="Details" component={Details} />
//       </Stack.Navigator>
    
// }</NavigationContainer>
{/* <Splash/>

  ); */}
}




