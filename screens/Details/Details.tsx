import { StatusBar } from 'expo-status-bar';
import { Button,Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Styles from './Styles'
export default function DetailsScreen({ navigation }: any) {
    return (
      <View style={Styles.container}>
        <Text style={Styles.text}>Details Screen</Text>
        <Button
        title="Go Back"
        onPress={() => navigation.goBack()}
      />
        <Button
        title="Go Home"
        onPress={() => navigation.navigate('Home')}
      />

      </View>
      
    );
  }