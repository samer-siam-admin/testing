import { StyleSheet} from 'react-native';

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '000',
      alignItems: 'center',
      justifyContent: 'center',

        },

        text:{
            color:'red',
            fontSize:40
        },

        input:{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          padding: "8px",
          position: "static",
          width: "380px",
          height: "51px",
          left: "0px",
          top: "27px",
          background: "#FFFFFF",
          border: "1px solid #DB3E76",
          boxSizing: "border-box",
          borderRadius: "30px",
          flex: "none",
          order: 1,
          flexGrow: 0,
          margin: "10px 0px"
        }
  });
  export default styles